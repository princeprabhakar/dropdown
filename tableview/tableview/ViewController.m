//
//  ViewController.m
//  tableview
//
//  Created by Prince Prabhakar on 05/10/15.
//  Copyright (c) 2015 Prince Prabhakar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *unihde;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;

@end

@implementation ViewController
NSArray *cellData;
@synthesize unihde;
@synthesize tableview;
@synthesize imageview;
- (IBAction)unhide:(id)sender {
    tableview.hidden=NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
   
    cellData=@[@"Prince",@"Chandigarh",@"Param",@"Pehowa",@"Mani",@"Mohali"];
    [tableview reloadData];
    tableview.hidden=YES;
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{

    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"sel"];
   
   cell.textLabel.text= cellData[indexPath.row];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return cellData.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *str=cellData[indexPath.row];
    [unihde setTitle:str forState:UIControlStateNormal];
    tableview.hidden=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
